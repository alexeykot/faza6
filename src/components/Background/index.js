import React from 'react';
import { withRouter } from 'react-router-dom';

// import background from 'assets/images/bg-black.png';
// import bgWall from 'assets/images/wall.jpg';

import * as S from './styled';

const blurPaths = [
  '/price',
];

const backgrounds = {
  default: 'https://sun9-99.userapi.com/c851520/v851520904/1c35b3/s522-dSIuGc.jpg',
  shop: 'https://sun9-14.userapi.com/c857624/v857624516/779e2/qz1qru1RdJw.jpg',
};

const srcPaths = {
  '/barbers': backgrounds.default,
  '/price': backgrounds.default,
  '/our-work': backgrounds.default,
  '/main': backgrounds.default,
  '/shop': backgrounds.shop,
  '/interior': backgrounds.default,
};

// const src = src={srcPaths[props.location.pathname]} use in src
const Background = (props) => {
  const isBlurred = blurPaths.includes(props.location.pathname);
  return (
    <S.BackgroundImage bgUrl={srcPaths[props.location.pathname]} isBlurred={isBlurred}>
      <S.ShadowMask />
    </S.BackgroundImage>
  );
}

export default withRouter(Background);