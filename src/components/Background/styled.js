import styled from 'styled-components';

export const BackgroundImage = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  left: 0;
  top: 0;
  /* background-image: url('https://sun9-31.userapi.com/c852220/v852220218/1c4fef/wbCJgGd5s_I.jpg'); */
  /* background-image: url('https://sun9-23.userapi.com/c858028/v858028419/6e8c5/Xdt9jqHliJ4.jpg'); */
  background-image: url(${({ bgUrl }) => bgUrl});
  background-size:  cover;
  overflow: hidden;
  background-position: 50% 50%;
  background-repeat: no-repeat;
  filter: ${({ isBlurred }) => isBlurred ? 'blur(10px)' : 'none'};
`;

export const ShadowMask = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.73);
`;