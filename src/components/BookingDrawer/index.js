import React from 'react';

import * as S from './styled';

const BookingDrawer = ({ isOpen, onClose }) => (
  <React.Fragment>
    <S.Container isOpen={isOpen}>
      <iframe
        title="faza"
        height="100%"
        width="100%"
        scrolling="no"
        frameBorder="0"
        allowtransparency="true"
        id="ms_booking_iframe"
        src="https://w236386.yclients.com/">
    
        </iframe>
    </S.Container>
    <S.ShadowMask isOpen={isOpen} onClick={onClose} />
  </React.Fragment>
);

export default BookingDrawer;