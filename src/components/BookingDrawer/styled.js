import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  width: 500px;
  background-color: white;
  position: fixed;
  right: ${({ isOpen }) => isOpen ? 0 : '-500px'};
  z-index: 1111111111;
  transition: all 200ms linear;
`;

export const ShadowMask = styled.div`
  height: 100vh;
  width: 100%;
  position: fixed;
  background-color: transparent;
  z-index: 111111111;
  display: ${({ isOpen }) => isOpen ? 'block' : 'none'}
`;