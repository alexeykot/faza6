import React from 'react';
import { withRouter } from 'react-router-dom';

import { ReactComponent as InstagramIcon } from 'assets/icons/instagram.svg';
import { ReactComponent as LocationIcon } from 'assets/icons/location2.svg';

import * as S from './styled';

const displayPaths = ['/main']

const Footer = (props) => {
  return (
    <S.Container notDisplay={!displayPaths.includes(props.location.pathname)}>
      <S.RowWrapper>
        <LocationIcon />
        <a href="https://www.google.com/maps/place/%D0%91%D0%B0%D1%80%D0%B1%D0%B5%D1%80%D1%88%D0%BE%D0%BF+Faza/@53.9010485,27.554909,16z/data=!4m8!1m2!2m1!1z0YTQsNC30LAg0LHQsNGA0LHQtdGA0YjQvtC_!3m4!1s0x0:0x99dc66245ccc5e91!8m2!3d53.9013167!4d27.5574875" rel="noopener noreferrer" target="_blank">г. Минск ул. Ленина 6 (Вход со двора) </a>
      </S.RowWrapper>
      <S.RowWrapper>
        <InstagramIcon />
        <a href="https://www.instagram.com/faza_barbershop" rel="noopener noreferrer" target="_blank">faza_barbershop</a>
      </S.RowWrapper>
    </S.Container>
  );
}

export default withRouter(Footer);