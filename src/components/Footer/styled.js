import styled from 'styled-components';
import { ReactComponent as location } from 'assets/icons/location.svg';
import { ReactComponent as instagram } from 'assets/icons/instagram.svg';

export const Container = styled.div`
  z-index: 100;
  display: flex;
  width: 100vw;
  height: 50px;
  padding: 0 80px;
  box-sizing: border-box;
  background-color: transparent;
  color: white;
  position: fixed;
  bottom: 0;
  left: 0;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 960px) {
    flex-direction: column;
    padding: 0 70px;
    height: 100px;
    align-items: center;
    text-align: center;
    justify-content: space-evenly;
    display: ${({ notDisplay }) => notDisplay && 'none'};
    /* z-index: 11111; */
  }
`;

export const RowWrapper = styled.span`
  display: flex;
  justify-content: center;
  svg {
    width: 20px;
    height: 20px;
    fill: white;
    margin-right: 10px;
    @media (max-width: 960px) {
      margin-right: 5px;
    }
  }
  a {
    cursor: pointer;
    color: white;
    text-decoration: none;
  }
  :hover {
    svg {
      fill: orange;
    }
    a {
      color: orange;
    }
  }
`;

export const LocationIcon = styled(location)``;
export const InstagramIcon = styled(instagram)``;