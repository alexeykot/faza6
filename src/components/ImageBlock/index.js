import React, { Component } from 'react';
import LazyLoad from 'react-lazyload';

import * as S from './styled';

class ImageBlock extends Component {

  state = {
    isHovered: false,
    isOpen: false
  }

  onHover = () => this.setState({ isHovered: true });
  onBlur = () => this.setState({ isHovered: false });

  // bookBarber = () => {
  //   const { barberId } = this.props;
  //   console.log(barberId);
  //   if (barberId) {
  //     window.location.href = `https://n236386.yclients.com/company:232710?o=${barberId}`;
  //   }
  // }

  onClick = () => {
    this.setState({ isOpen: !this.state.isOpen });
    // this.bookBarber();
  }

  render() {
    const { source, title, width } = this.props;
    const { isHovered, isOpen } = this.state;
    return (
      <LazyLoad>
      <S.Container style={{ backgroundImage: `url(${source})`}} isOpen={isOpen} onClick={this.onClick} width={width} onMouseEnter={this.onHover} onMouseLeave={this.onBlur}>
        <S.ShadowMaskTop isHovered={isHovered} />
        <S.ShadowMaskBottom isHovered={isHovered} />
        {/* <S.Image src={source} /> */}
        <S.Title isOpen={isOpen} isHovered={isHovered}>{title}</S.Title>
      </S.Container>
      </LazyLoad>
    );
  }
}


export default ImageBlock;
