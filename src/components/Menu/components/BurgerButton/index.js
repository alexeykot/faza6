import React from 'react';

import * as S from './styled';

const BurgerButton = ({ onClick, isMenuOpen }) => (
  <S.Container onClick={onClick}>
    <S.FirstRow isMenuOpen={isMenuOpen}/>
    <S.SecondRow isMenuOpen={isMenuOpen} />
    <S.ThirdRow isMenuOpen={isMenuOpen} />
  </S.Container>
);

export default BurgerButton;
