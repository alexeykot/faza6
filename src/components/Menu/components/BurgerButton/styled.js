import styled from 'styled-components';

const Row = styled.div`
  width: 26px;
  height: 3px;
  background-color: white;
  margin: 6px 0;
  transition: 0.4s;
`;

export const Container = styled.div`
  display: inline-block;
  cursor: pointer;
  display: none;
    @media (max-width: 960px) {
    display: block;
    z-index: 16;
  }
`;

export const FirstRow = styled(Row)`
  transform: ${({ isMenuOpen }) => isMenuOpen && 'rotate(-45deg) translate(-5px, 5px)'};
`;

export const SecondRow = styled(Row)`
  opacity: ${({ isMenuOpen }) => isMenuOpen ? 0 : 1};
`;

export const ThirdRow = styled(Row)`
  transform: ${({ isMenuOpen }) => isMenuOpen && 'rotate(45deg) translate(-8px, -8px)'};
`;