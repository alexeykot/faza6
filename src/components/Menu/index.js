import React from 'react';
import { withRouter } from 'react-router-dom';

import BurgerButton from './components/BurgerButton';
import logo from 'assets/images/logo-white.png';

import * as S from './styled';
import PrimaryButton from '../PrimaryButton';


const menuItems = [
  {
    name: 'Главная',
    path: 'main',
  },
  {
    name: 'Мастера',
    path: 'barbers',
  },
  {
    name: 'Услуги',
    path: 'price',
  },
  {
    name: 'Портфолио',
    path: 'our-work',
  },
  {
    name: 'Интерьер',
    path: 'interior',
  },
  {
    name: 'Магазин',
    path: 'shop',
  },
];

const displayPaths = ['/main']

class Menu extends React.Component {

  state = {
    isMenuOpen: false,
  }

  toggleMenu = () => this.setState({ isMenuOpen: !this.state.isMenuOpen });

  hideMenu = () => this.setState({ isMenuOpen: false });

  goToMain = () => this.props.history.push('/main');

  goToBooking = () => window.location.href = "https://w236386.yclients.com/";

  render() {

    const { location } = this.props;
    const { isMenuOpen } = this.state;
    return (
      <S.Container>
        <S.Logo src={logo} onClick={this.goToMain}/>
        <BurgerButton  onClick={this.toggleMenu} isMenuOpen={isMenuOpen} />
        <S.TabsWrapper isMenuOpen={isMenuOpen}>
          {menuItems.map(item => (<S.LinkText key={item.path} onClick={this.hideMenu} isLinkActive={location.pathname === `/${item.path}`} to={`${item.path}`}>{item.name}</S.LinkText>))}
          {isMenuOpen && <PrimaryButton onClick={this.goToBooking}/>}
        </S.TabsWrapper>
        <S.InfoBlock notDisplay={!displayPaths.includes(location.pathname)}>
          <S.TimeText>Пн-Вс 10:00-21:00</S.TimeText>
          <S.TelLink href="tel:80447827749">+375 44 782 77 49</S.TelLink>
        </S.InfoBlock>
      </S.Container>
    );
  }
}

export default withRouter(Menu);
