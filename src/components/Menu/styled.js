import styled from 'styled-components';
// import { ReactComponent as burger } from 'assets/icons/burger.svg';
import { NavLink } from 'react-router-dom';

export const Container = styled.header`
  background-color: transparent;
  justify-content: space-between;
  padding: 0 80px;
  box-sizing: border-box;
  align-items: center;
  height: 150px;
  display: flex;
  z-index: 1111;
  /* position: sticky; */
  position: fixed;
  width: 100vw;
  top: 0;
  @media (max-width: 960px) {
    padding: 0 30px;
    height: 70px;
  }
`;

export const TabsWrapper = styled.div`
  width: 50%;
  height: 100%;
  background-color: transparent;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  justify-self: center;
  @media (max-width: 960px) {
    display: ${({ isMenuOpen }) => isMenuOpen ? 'flex' : 'none'};
    position: absolute;
    background-color: black;
    width: 100vw;
    height: 100vh;
    box-sizing: border-box;
    flex-direction: column;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: 15;
    justify-content: center;
    a {
      margin-bottom: 30px;
      font-size: 16px;
    }
    button {
      margin-top: 50px;
    }
  }
`;

export const Logo = styled.img`
  height: 40px;
  width: 100px;
  -webkit-user-select: none;
  :hover {
    cursor: pointer;
  }
  @media (max-width: 960px) {
    z-index: 16;
    height: 30px;
    width: 80px;
  }
`;

export const InfoBlock = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  @media (max-width: 960px) {
    display: ${({ notDisplay }) => notDisplay && 'none'};
    position: absolute;
    left: 50%;
    margin-left: -70px;
    top: 100px;
  }
`;

export const TelLink = styled.a`
  font-size: 14px;
  font-weight: bold;
  color: white;
  text-decoration: none;
  :hover {
    color: orange;
  }
`;

export const TimeText = styled.span`
  font-size: 14px;
  font-weight: bold;
  color: white;
`;

export const LinkText = styled(NavLink)`
  font-size: 14px;
  font-weight: bold;
  color: ${({ isLinkActive }) => isLinkActive ? 'orange' : 'white'};
  text-decoration: none;
  -webkit-user-select: none;
  
  :any-link {
    cursor: pointer;
  }
  :hover {
    color: orange;
  }
`;

