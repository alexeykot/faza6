import React from 'react';

import PrimaryButton from 'components/PrimaryButton'

import * as S from './styled';

const photo = 'https://sun9-28.userapi.com/c850632/v850632971/1f319f/nRqAPPCtd4c.jpg';

const SaleModal = ({ isModalOpen, closeModal, onClick }) => {
  const onBookClick = () => {
    onClick();
    closeModal();
  }
  return (
    <S.Container isModalOpen={isModalOpen}>
      <S.CloseButton onClick={closeModal}>X</S.CloseButton>
      <S.LeftSection photo={photo} />
      <S.RightSection>
        <S.Text>
          Скидка на первое посещение 30%
        </S.Text>
        <PrimaryButton onClick={onBookClick} />
      </S.RightSection>
    </S.Container>
  );
}

export default SaleModal;
