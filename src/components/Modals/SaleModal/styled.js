import styled from 'styled-components';

export const Container = styled.div`
  position: absolute;
  /* left: 50%;
  top: 50%; */
  width: 600px;
  height: 400px;
  background-color: black;
  z-index: 111111;
  /* opacity: ${({ isModalOpen }) => isModalOpen ? 1 : 0.5}; */
  top: ${({ isModalOpen }) => isModalOpen ? '30%' : '-700px'};
  transition: all 0.7s linear;
  display: flex;
  @media (max-width: 960px) {
    width: 90%;
    height: 50%;
    top: ${({ isModalOpen }) => isModalOpen ? '25%' : '-700px'};
  }
`;

export const CloseButton = styled.div`
  position: absolute;
  top: -15px;
  right: -15px;
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: black;
  font-size: 25px;
  font-weight: 600;
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  user-select: none;
`;

export const Text = styled.span`
  font-size: 30px;
  color: black;
  text-align: center;
  margin-bottom: 30px;
  @media (max-width: 330px) {
    font-size: 25px;
  }
`;

export const LeftSection = styled.div`
  width: 50%;
  height: 100%;
  background-color: blue;
  background: url(${({ photo }) => photo});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  @media (max-width: 960px) {
    background-position: 55%;
  }
  @media (max-width: 330px) {
    width: 40%;
  }
`;

export const RightSection = styled.div`
  width: 50%;
  height: 100%;
  background-color: white;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media (max-width: 330px) {
    width: 60%;
  }
`;