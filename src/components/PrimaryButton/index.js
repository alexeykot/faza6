import React from 'react';

import * as S from './styled';

const PrimaryButton = ({ title, onClick }) => (
  <S.Container onClick={onClick}>
    {title}
  </S.Container>
);

PrimaryButton.defaultProps = {
  title: "Записаться",
  onClick: () => console.log('pressed'),
};

export default PrimaryButton;