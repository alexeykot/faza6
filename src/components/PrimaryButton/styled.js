import styled from 'styled-components';

export const Container = styled.button`
  /* background: #FF8A00; */
  background: orange;
  border-radius: 20px;
  padding: 11px 28px;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 19px;
  color: white;
  /* margin-top: 30px; */
  text-transform: uppercase;
  border: none;
  outline: none;
  cursor: pointer;
  -webkit-user-select: none;
`;