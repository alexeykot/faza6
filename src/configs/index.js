import React from 'react';

import { ReactComponent as ScissorsIcon } from 'assets/icons/scissors.svg';
import { ReactComponent as RazorIcon } from 'assets/icons/razor.svg';
import { ReactComponent as GelIcon } from 'assets/icons/gel.svg';
import { ReactComponent as ClockIcon } from 'assets/icons/clock.svg';

export const barbersConfig = [
  {
    imageUrl: 'https://sun9-32.userapi.com/c851536/v851536553/1e771a/Tr3HEa-TW6Y.jpg',
    name: "Мастер Дмитрий",
    barberId: '669804',
  },
  {
    imageUrl: 'https://sun9-17.userapi.com/c852220/v852220446/1c141a/n_xG85klphg.jpg',
    name: "Мастер Антон",
    barberId: '669804',
  },
  {
    imageUrl: 'https://sun9-10.userapi.com/c852220/v852220446/1c1410/S1CILZ9dp_w.jpg',
    name: "Мастер Дмитрий",
    barberId: '669804',
  },
  {
    imageUrl: 'https://sun9-28.userapi.com/c854420/v854420628/f6ba1/MGpRoPQ-WWU.jpg',
    name: "Мастер Денис",
    barberId: '669804',
  },
  {
    imageUrl: 'https://sun9-7.userapi.com/c851536/v851536553/1e7700/xjTl0_tOM5I.jpg',
    name: "Мастер Артем",
    barberId: '669804',
  },
  {
    imageUrl: 'https://sun9-52.userapi.com/c854420/v854420628/f6bab/vZvJaoL13gY.jpg',
    name: "Мастер Евгений",
    barberId: '669804',
  },
];

export const interiorConfig = [
  {
    imageUrl: 'https://sun9-31.userapi.com/c852220/v852220218/1c4fef/wbCJgGd5s_I.jpg',
  },
  {
    imageUrl: 'https://sun9-48.userapi.com/c851028/v851028438/1bd5bd/zDQ2r2n911c.jpg',
  },
  {
    imageUrl: 'https://sun9-44.userapi.com/c850120/v850120553/1eb39e/AbgxzdjwClU.jpg',
  },
  {
    imageUrl: 'https://sun9-7.userapi.com/c850120/v850120553/1eb3b2/Sa3r0iuWAW4.jpg',
  },
  {
    imageUrl: 'https://sun9-69.userapi.com/c850120/v850120553/1eb38a/1XYwBrEFQbo.jpg',
  },
  {
    imageUrl: 'https://sun9-17.userapi.com/c851028/v851028438/1bd5c7/CzUYLaWOt5k.jpg',
  },
];

export const portfolioConfig = [
  {
    imageUrl: 'https://sun9-28.userapi.com/c854420/v854420628/f6be2/Xe72_uaDNLI.jpg',
    name: "Мастер Дмитрий",
  },
  {
    imageUrl: 'https://sun9-69.userapi.com/c854420/v854420628/f6bec/tA7mhAf3tvU.jpg',
    name: "Мастер Антон",
  },
  {
    imageUrl: 'https://sun9-7.userapi.com/c854420/v854420628/f6c31/_OGHbZb2y10.jpg',
    name: "Мастер Евгений",
  },
  {
    imageUrl: 'https://sun9-59.userapi.com/c854420/v854420628/f6c13/Apt95tsf_y4.jpg',
    name: "Мастер Артем",
  },
  {
    imageUrl: 'https://sun9-34.userapi.com/c854420/v854420628/f6c00/5LFUEoIE9jQ.jpg',
    name: "Мастер Денис",
  },
  {
    imageUrl: 'https://sun9-40.userapi.com/c854420/v854420628/f6c27/iBqW9D-BYzs.jpg',
    name: "Мастер Евгений",
  },
  {
    imageUrl: 'https://sun9-70.userapi.com/c854420/v854420628/f6c43/q6DpKT2VRM0.jpg',
    name: "Мастер Евгений",
  },
  {
    imageUrl: 'https://sun9-63.userapi.com/c854420/v854420628/f6c39/CeOJAs4DLtU.jpg',
    name: "Мастер Евгений",
  },
  {
    imageUrl: 'https://sun9-61.userapi.com/c854420/v854420628/f6bf6/IBvUekaAxrY.jpg',
    name: "Мастер Дмитрий",
  },
];

export const categories = [
  {
    categoryName: 'Стрижка',
    description: 'Выполняется машинкой, ножницами, бритвой со сменными лезвиями (шаветкой), используются профессиональные косметические средства.',
    icon: <ScissorsIcon />,
    serviceTime: '~1 час',
    services: [
      {
        serviceName: 'СТРИЖКА МАШИНКОЙ',
        serviceDescription: 'Стрижка с применением машинки. В услугу входит мытьё головы, стрижка и укладка профессиональными средствами.',
        price: '25p',
      },
      {
        serviceName: 'СТРИЖКА',
        serviceDescription: 'Стрижка с применением машинки и ножниц. В услугу входит мытьё головы, стрижка и укладка профессиональными средствами.',
        price: '35p',
      },
      {
        serviceName: 'ДЕТСКАЯ СТРИЖКА',
        serviceDescription: 'Стрижка для молодых людей от 2 до 16 лет с применением машинки и ножниц. В услугу входит стрижка и укладка профессиональными средствами.',
        price: '30p',
      },
      {
        serviceName: 'УКЛАДКА / ОКАНТОВКА',
        serviceDescription: 'В услугу входит мытьё головы и укладка профессиональными средствами.',
        price: '10p',
      },
    ]
  },
  {
    categoryName: 'Бритьё',
    description: 'Выполняется машинкой, ножницами, бритвой со сменными лезвиями (шаветкой), используется горячий компресс и  профессиональные косметические средства.',
    icon: <RazorIcon />,
    serviceTime: '~45 мин',
    services: [
      {
        serviceName: 'СТРИЖКА БОРОДЫ',
        serviceDescription: 'Моделирование бороды и усов с использование машинки и ножниц.',
        price: '25p',
      },
      {
        serviceName: 'БРИТЬЕ ОПАСНОЙ БРИТВОЙ',
        serviceDescription: 'Классическое бритьё опасной бритвой со сменными лезвиями (Шаветкой)',
        price: '35p',
      },
    ]
  },
  {
    categoryName: 'Тонирование волос',
    description: 'Выполняется с использованием профессиональных безаммиачных тонирующих средств.',
    icon: <GelIcon />,
    serviceTime: '~45 мин',
    services: [
      {
        serviceName: 'ТОНИРОВАНИЕ БОРОДЫ',
        serviceDescription: 'Камуфлирование седины, окрашивание волос в желаемый цвет.',
        price: '25p',
      },
      {
        serviceName: 'ТОНИРОВАНИЕ ГОЛОВЫ',
        serviceDescription: 'Камуфлирование седины, окрашивание волос в желаемый цвет.',
        price: '30p',
      },
    ]
  },
  {
    categoryName: 'Комбо',
    description: 'Комплекс услуг на выбор клиента состоящий из стрижки, бритья и тонирования.',
    icon: <ClockIcon />,
    serviceTime: '~1.5 часа',
    services: [
      {
        serviceName: 'СТРИЖКА БОРОДЫ И СТРИЖКА МАШИНКОЙ',
        serviceDescription: '',
        price: '50p',
      },
      {
        serviceName: 'СТРИЖКА БОРОДЫ И СТРИЖКА',
        serviceDescription: '',
        price: '55p',
      },
      {
        serviceName: 'СТРИЖКА ОТЕЦ + СЫН',
        serviceDescription: '',
        price: '55p',
      },
      {
        serviceName: 'СТРИЖКА ДРУГ + ДРУГ',
        serviceDescription: '',
        price: '60p',
      },
      {
        serviceName: 'СТРИЖКА МАШИНКОЙ + ТОНИРОВАНИЕ БОРОДЫ',
        serviceDescription: '',
        price: '45p',
      },
      {
        serviceName: 'СТРИЖКА + ТОНИРОВАНИЕ БОРОДЫ',
        serviceDescription: '',
        price: '55p',
      },
      {
        serviceName: 'СТРИЖКА МАШИНКОЙ + ТОНИРОВАНИЕ ГОЛОВЫ',
        serviceDescription: '',
        price: '45p',
      },
      {
        serviceName: 'СТРИЖКА + ТОНИРОВАНИЕ ГОЛОВЫ',
        serviceDescription: '',
        price: '55p',
      },
    ]
  },
];
