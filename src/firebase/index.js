import firebase from 'firebase/app';

import 'firebase/storage';

var firebaseConfig = {
  apiKey: "AIzaSyDUFxzhx34R4Ju1eL-Dl9s9nUsmHtGjDaY",
  authDomain: "faza6-2fc5d.firebaseapp.com",
  databaseURL: "https://faza6-2fc5d.firebaseio.com",
  projectId: "faza6-2fc5d",
  storageBucket: "faza6-2fc5d.appspot.com",
  messagingSenderId: "275698431339",
  appId: "1:275698431339:web:913df190e2690cd6edc82e"
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export default storage;