import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { ThemeProvider } from 'styled-components';
import configureStore from 'redux/configureStore';

import theme from './theme';
import './index.css';


import AppRouter from './router'

const store = configureStore();
ReactDOM.render(
  <Provider store={store}>
  <ThemeProvider theme={theme}>
    <AppRouter />
  </ThemeProvider>
  </Provider>,
  document.getElementById('root'),
);

