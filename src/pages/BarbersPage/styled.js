import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  padding: 0px 80px;
  box-sizing: border-box;
  position: relative;
  &::-webkit-scrollbar {
        display: none;
    }
`;

export const ProgressContainer = styled.div`
  width: 100%;
  height: 4px;
  background: #ccc;
  position: absolute;
  top: 0;
  z-index: 12345;
  div {
    height: 4px;
    background: orange;
    width: 0%;
  }
  @media (max-width: 960px) {
    display: none;
  }
`;

export const MainBlock = styled.div`
  z-index: 11;
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  will-change: transform;
  overflow-x: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
  &.active {
    cursor: grabbing;
    cursor: -webkit-grabbing;
  }
  @media (max-width: 960px) {
    display: grid;
    grid-template-rows: 1fr 1fr 1fr;
    grid-template-columns: 1fr 1fr;
    top: 70px;
    height: calc(100vh - 70px);
  }
`;

export const RightArrowButton = styled.div`
  width: 100px;
  height: 50px;
  z-index: 11;
  background-color: transparent;
  position: fixed;
  top: 50%;
  right: 0px;
  cursor: pointer;
  svg {
    fill: white;
    width: 40px;
    height: 40px;
  }
  :active {
    svg {
      fill: orange;
    }
  }
  @media (max-width: 960px) {
    display: none;
  }
`;

export const LeftArrowButton = styled.div`
  width: 100px;
  height: 50px;
  z-index: 11;
  background-color: transparent;
  position: fixed;
  top: 50%;
  left: 50px;
  cursor: pointer;
  svg {
    fill: white;
    transform: rotate(180deg);
    width: 40px;
    height: 40px;
  }
  :active {
    svg {
      fill: orange;
    }
  }
  @media (max-width: 960px) {
    display: none;
  }
`;
