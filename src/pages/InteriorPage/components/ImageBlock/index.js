import React from 'react';
import LazyLoad from 'react-lazyload';

import * as S from './styled';

class ImageBlock extends React.Component {

  state = {
    isOpen: false,
    isHovered: false,
  }

  onHover = () => this.setState({ isHovered: true });
  onBlur = () => this.setState({ isHovered: false });

  openBlock = () => {
    const { openImage, index } = this.props;
    openImage(index);
  }

  render() {
    const { isOpened, source } = this.props;
    const { isHovered } = this.state;
    return (
      <LazyLoad>
        <S.Container
          index={this.props.index}
          onMouseEnter={this.onHover}
          onMouseLeave={this.onBlur}
          isOpen={isOpened}
          onClick={this.openBlock}
          style={{ backgroundImage: `url(${source})`}}
        >
          {/* {!isOpened && <S.ShadowMask />} */}
          <S.ShadowMaskTop isHovered={isHovered || isOpened} />
          <S.ShadowMaskBottom isHovered={isHovered || isOpened} />
        </S.Container>
      </LazyLoad>
    );
  }
}

export default ImageBlock;
