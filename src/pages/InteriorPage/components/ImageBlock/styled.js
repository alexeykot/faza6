import styled from 'styled-components';

export const Container = styled.div`
  flex: 1;
  position: relative;
  /* background-image: url(${({ url }) => url}); */
  background-repeat: no-repeat;
  background-size: cover;
  background-position: 50% 50%;
  min-width: ${({ isOpen }) => isOpen ? '60vw' : '0'};
  cursor: pointer;
  transition: all .5s linear;
  @media (max-width: 960px) {
    background-size: cover;
    min-width: ${({ isOpen }) => isOpen ? '100vw' : '0'};
    min-height: ${({ isOpen }) => isOpen ? '100vh' : '0'};
    background-position: auto;
  }
`;

export const ShadowMask = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.73);
`;

export const ShadowMaskTop = styled.div`
  width: 100%;
  height: 100%;
  background: ${({ isHovered }) => isHovered ? 'linear-gradient(180deg, rgba(0, 0, 0, 0.7) 9.38%, rgba(0, 0, 0, 0) 44.27%);' : 'rgba(0, 0, 0, 0.7)'};
  /* opacity: ${({ isHovered }) => isHovered ? 0 : 0.4}; */
  position: absolute;
  top: 0;
  left: 0;
  @media (max-width: 960px) {
    background: linear-gradient(180deg, rgba(0, 0, 0, 0.7) 9.38%, rgba(0, 0, 0, 0) 44.27%);
    transform: rotate(180deg); 
  }
`;

export const ShadowMaskBottom = styled.div`
  width: 100%;
  height: 100%;
  background: ${({ isHovered }) => isHovered ? 'linear-gradient(180deg, rgba(0, 0, 0, 0.7) 9.38%, rgba(0, 0, 0, 0) 44.27%);' : 'none'};
  /* opacity: ${({ isHovered }) => isHovered ? 0 : 0.4}; */
  transform: rotate(180deg);
  position: absolute;
  top: 0;
  left: 0;
  @media (max-width: 960px) {
    background: none;
  }
`;