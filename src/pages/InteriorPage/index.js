import React from 'react';

import { interiorConfig } from 'configs';
import ImageBlock from './components/ImageBlock';

import * as S from './styled';


class InteriorPage extends React.Component {

  state = {
    openedImage: -1,
  };

  openImage = (index) => {
    const { openedImage } = this.state;
    if (openedImage === index) {
      this.setState({ openedImage: -1 });
      return ;
    }
    this.setState({ openedImage: index });
  }

  render() {
    const { openedImage } = this.state;
    return (
      <S.Container>
        <S.MainBlock length={interiorConfig.length}>
          {interiorConfig.map((photo, index) => (
            <ImageBlock
              key={photo.imageUrl}
              openImage={() => this.openImage(index)}
              isOpened={openedImage === index}
              index={index}
              source={photo.imageUrl}
            />
          ))}
        </S.MainBlock>
      </S.Container>
    );
  }
}

export default InteriorPage;