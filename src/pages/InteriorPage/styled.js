import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  padding: 0px 80px;
  box-sizing: border-box;
  position: relative;
  &::-webkit-scrollbar {
        display: none;
    }
`;

export const MainBlock = styled.div`
  z-index: 11;
  width: 100vw;
  height: 100vh;
  display: grid;
  grid-template-columns: ${({ length }) => `repeat(${length}, 1fr)`};
  @media (max-width: 960px) {
    display: grid;
    grid-template-rows: 1fr 1fr 1fr;
    grid-template-columns: 1fr 1fr;
    position: absolute;
    top: 70px;
    height: calc(100vh - 70px);
  }
`;
