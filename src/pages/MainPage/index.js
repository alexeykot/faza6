import React from 'react';
import LazyLoad from 'react-lazyload';
import { connect } from 'react-redux';
import { ReactComponent as PlayIcon } from 'assets/icons/play-button.svg';

import PrimaryButton from 'components/PrimaryButton';
import BookingDrawer from 'components/BookingDrawer';
import SaleModal from 'components/Modals/SaleModal';

import { requestLoadDiscount } from 'redux/app';
import * as S from './styled';

class MainPage extends React.Component {
  state = {
    isVideoPlay: false,
    isVideoMuted: false,
    controlsVisible: false,
    isDrawerOpen: false,
    isLoading: false,
    isModalOpen: false,
  };

  componentDidMount() {
    window.addEventListener('resize', this.checkWindowSize);
    setTimeout(() => this.setState({ isModalOpen: true }), 500);
  //  this.props.requestLoadDiscount();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.checkWindowSize);
    this.props.requestLoadDiscount();
  }

  playVideo = () => {
    // const { isVideoPlay } = this.state;
    this.setState({ isVideoPlay: true, isVideoMuted: false });
  }

  closeVideo = () => {
    this.setState({ isVideoPlay: false, controlsVisible: false  })
  }

  muteVideo = () => this.setState({ isVideoMuted: !this.state.isVideoMuted });

  openDrawer = () => {
    if (window.innerWidth < 960) {
      window.location.href = "https://w236386.yclients.com/";
    } else {
      this.setState({ isDrawerOpen: true });
    }
  };

  closeDrawer = () => {
    this.setState({ isDrawerOpen: false });
  }

  checkWindowSize = () => {
    if (window.innerWidth < 960) {
      this.setState({ isDrawerOpen: false });
    }
  }

  onLoadData = () => {
    this.setState({ controlsVisible: true });
  }

  closeModal = () => {
    this.props.requestLoadDiscount();
  }

  render() {
    const { isVideoPlay, isVideoMuted, controlsVisible, isDrawerOpen, isModalOpen } = this.state;
    const { loadedDiscount } = this.props;
    console.log(isModalOpen);
    return (
      <S.Container>
        {!loadedDiscount && <SaleModal onClick={this.openDrawer} isModalOpen={isModalOpen} closeModal={this.closeModal} />}
        <BookingDrawer onClose={this.closeDrawer} isOpen={isDrawerOpen} />
        <S.DescriptionBlock>
          <h1>{'ВARBERSHOP & STORE'}</h1>
          <div>{'Мужские cтрижки и бритьё.'}</div>
          <span style={{ marginBottom: '30px'}}>{'Новая фаза твоего стиля!'}</span>
          <PrimaryButton onClick={this.openDrawer} />
        </S.DescriptionBlock>
        <LazyLoad>
        {isVideoPlay &&
          <S.VideoContainer>
            <S.Video id="video" onLoadedData={this.onLoadData} src="https://firebasestorage.googleapis.com/v0/b/faza6-2fc5d.appspot.com/o/IMG_4684.MOV?alt=media&token=368a73e8-1d14-46e1-bc22-323563736205" muted={isVideoMuted} autoPlay loop preload="none" />
            {controlsVisible &&
              <React.Fragment>
                <S.CloseButton onClick={this.closeVideo} />
                {isVideoMuted ? <S.MutedButton onClick={this.muteVideo} /> : <S.VolumeButton onClick={this.muteVideo} />}
              </React.Fragment>
            }
          </S.VideoContainer>}
        </LazyLoad>
        <PlayIcon onClick={this.playVideo} />
      </S.Container>
    );
  }
}

const mapStateToProps = ({ app }) => ({
  loadedDiscount: app.loadedDiscount,
});

const mapDispatchToProps = {
  requestLoadDiscount,
}
export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
