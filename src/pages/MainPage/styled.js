import styled from 'styled-components';
import { ReactComponent as CloseIcon } from 'assets/icons/close.svg';
import { ReactComponent as SpeakerIcon } from 'assets/icons/speaker.svg';
import { ReactComponent as MuteIcon } from 'assets/icons/mute.svg';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 100vh;
  svg {
    width: 48px;
    height: 48px;
    fill: white;
    position: absolute;
    right: 120px;
    bottom: 50px;
    :hover {
      fill: orange;
      cursor: pointer;
    }
    @media (max-width: 960px) {
      display: none;
    }
  }
  @media (max-width: 960px) {
      position: relative;
      overflow: hidden;
    }
`;

export const DescriptionBlock = styled.div`
  z-index: 13;
  color: white;
  display: flex;
  flex-direction: column;
  /* justify-content: center; */
  align-items: center;
  text-align: center;
  @media (max-width: 960px) {
      padding: 0px 90px;
      overflow: hidden;
      position: fixed;
  }
    h1 {
    font-style: normal;
    font-weight: 500;
    font-size: 40px;
    line-height: 47px;
    @media (max-width: 960px) {
      font-style: normal;
      font-weight: 500;
      font-size: 28px;
      line-height: 33px;
  }
    }
    div {
    font-style: normal;
    font-weight: 300;
    font-size: 22px;
    line-height: 26px;
    text-align: center;
    letter-spacing: 0.05em;
    @media (max-width: 960px) {
      font-weight: 300;
      font-size: 20px;
      line-height: 23px;
      margin-bottom: 70px;
  }

  @media (max-width: 330px) {
      margin-bottom: 20px;
  }
  
    }
    span {
    font-style: normal;
    font-weight: 300;
    font-size: 22px;
    line-height: 26px;
    text-align: center;
    letter-spacing: 0.05em;
    @media (max-width: 960px) {
      display: none;
  }
  
    }
`;

export const Video = styled.video`
  width: 100vw;
`;

export const VideoContainer = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1234;
  overflow: hidden;
  /* pointer-events: none; */
  /* svg {
    width: 48px;
    height: 48px;
    fill: red;
    z-index: 1231232;
  } */
`;

export const CloseButton = styled(CloseIcon)`
  width: 48px;
  height: 48px;
  fill: red;
  z-index: 1231232;
  position: absolute;
  right: 8%;
`;

export const VolumeButton = styled(SpeakerIcon)`
  width: 48px;
  height: 48px;
  fill: red;
  z-index: 1231232;
  position: absolute;
  left: 8%;
  cursor: pointer;
`;

export const MutedButton = styled(MuteIcon)`
  width: 48px;
  height: 48px;
  fill: red;
  z-index: 1231232;
  position: absolute;
  left: 8%;
  cursor: pointer;
`;

