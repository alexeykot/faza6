import React from 'react';

import * as S from './styled';

const NotFoundPage = () => (
  <S.Container>
    <S.Title>Данной страницы не существует</S.Title>
    <S.BackgroundImage>
      <S.ShadowMask>
      </S.ShadowMask>
    </S.BackgroundImage>
  </S.Container>
);

export default NotFoundPage;
