import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  height: 100vh;
  background-color: red;
`;

export const BackgroundImage = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  left: 0;
  top: 0;
  /* background-image: url('https://sun9-31.userapi.com/c852220/v852220218/1c4fef/wbCJgGd5s_I.jpg'); */
  background-image: url('https://sun9-99.userapi.com/c851520/v851520904/1c35b3/s522-dSIuGc.jpg');
  /* background-image: url(${({ bgUrl }) => bgUrl}); */
  background-size:  cover;
  overflow: hidden;
  background-position: 50% 50%;
  background-repeat: no-repeat;
  filter: ${({ isBlurred }) => isBlurred ? 'blur(10px)' : 'none'};
`;

export const ShadowMask = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.73);
`;

export const Title = styled.div`
  color: white;
  z-index: 12; 
  margin: auto;
  font-size: 44px;
  line-height: 48px;
  font-weight: 600;
  @media (max-width: 960px) {
      font-size: 30px;
      text-align: center;
    }
`;