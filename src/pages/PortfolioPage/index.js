import React from 'react';

import ImageBlock from 'components/ImageBlock';
import { ReactComponent as ArrowIcon } from 'assets/icons/arrow.svg';
import { portfolioConfig } from 'configs'

import * as S from './styled';

class PortfolioPage extends React.Component {

  state = {
    scrolled: false,
    isRightActive: true,
    isLeftActive: false,
  }

  componentDidMount() {
    const slider = document.getElementById('block');
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener('scroll', this.listenToScroll);
    slider.addEventListener('mousedown', (e) => {
      isDown = true;
      slider.classList.add('active');
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e) => {
      if(!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 2; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  }
  
  componentWillUnmount() {
    const slider = document.getElementById('block');
    slider.removeEventListener('scroll', this.listenToScroll)
  }

  indicateScroll = () => {
    const a = document.getElementById('block');
    const width = a.scrollWidth - a.clientWidth;
    const scrolled = (a.scrollLeft / width) * 100;
    document.getElementById("indicator").style.width = scrolled + "%";
  }

  listenToScroll = (e) => {
    const a = document.getElementById('block');
  
    const width = a.scrollWidth - a.clientWidth
  
    this.setState({ isLeftActive: !!a.scrollLeft})

    const scrolled = a.scrollLeft === width;
  
    this.setState({
      scrolled: scrolled,
      isRightActive: !scrolled,
    })
  }

  scrollTo = (scrolled) => {
    const a = document.getElementById('block');
    // const { scrolled } = this.state;
    const scrollWidthRight = a.scrollLeft + (a.scrollWidth / 3);
    const scrollWidthLeft = a.scrollLeft - (a.scrollWidth / 3);
    a.scrollTo({
      left: scrolled ? scrollWidthLeft : scrollWidthRight,
      behavior: 'smooth',
    });
    // this.setState({ scrolled: !scrolled });
  }

  render() {
    const { isRightActive, isLeftActive } = this.state;
    return (
      <S.Container>
        <S.ProgressContainer>
          <div id="indicator"></div>
        </S.ProgressContainer>
        <S.MainBlock onScroll={this.indicateScroll} id="block">
          {portfolioConfig.map(photo => <ImageBlock key={photo.imageUrl} source={photo.imageUrl} />)}
        </S.MainBlock>
        {isRightActive &&
          <S.RightArrowButton onClick={() => this.scrollTo(false)}>
            <ArrowIcon />
          </S.RightArrowButton>}
        {isLeftActive &&
          <S.LeftArrowButton onClick={() => this.scrollTo(true)}>
            <ArrowIcon />
          </S.LeftArrowButton>}
      </S.Container>
    );
  }
}

export default PortfolioPage;
