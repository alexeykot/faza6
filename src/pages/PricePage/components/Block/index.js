import React, { Fragment, Component } from 'react';

import PrimaryButton from 'components/PrimaryButton';
import { ReactComponent as ArrowIcon } from 'assets/icons/arrow.svg';
import { ReactComponent as XMarkIcon } from 'assets/icons/x-mark.svg';
import ServiceBlock from '../ServiceBlock';
import * as S from './styled';

class Block extends Component {
  state = {
    isChecked: false,
    isScrollable: false,
  };

  // componentDidMount() {
  //   const block = document.getElementById('block');
  //   block.addEventListener('scroll', () => this.setState({ isScrollable: false }))
  // }
  componentDidUpdate(prevProps, prevState) {
    if (this.state.isChecked !== prevState.isChecked) {
      if (this.wrapper.scrollHeight > this.wrapper.clientHeight) {
        this.setState({ isScrollable: true })
      }
    }
  }

  getInputRef = (node) => this.wrapper = node;

  expandCategory = () => {
    this.setState({ isChecked: true });
    if (this.wrapper.scrollHeight > this.wrapper.clientHeight) {
      this.setState({ isScrollable: true })
    }
  }

  closeCategory = () => {
    this.setState({ isChecked: false });
  }
  render() {
    const { openDrawer } = this.props;
    const { icon, categoryName, description, serviceTime, services } = this.props.category;
    const { isChecked, isScrollable } = this.state;
    return (
      <Fragment>
        <S.Container isChecked={isChecked}>
          {icon}
          {isChecked && <XMarkIcon className="xmark" onClick={this.closeCategory} />}
          <S.ContentWrapper id="block" isChecked={isChecked} ref={this.getInputRef}>
            {!isChecked ? 
            <Fragment>
              <S.Title>
                {categoryName}
              </S.Title>
              {description}
              <S.ServiceTime>Время <span>{serviceTime}</span></S.ServiceTime>
            </Fragment> :
            <Fragment>
              {services.map((service, index) => (<ServiceBlock isLast={services.length === index + 1} service={service} />))}
            </Fragment>
          }
          </S.ContentWrapper>
          {/* {isChecked && isScrollable &&
          <Fragment>
            <S.ScrollText>Пролистайте вниз</S.ScrollText>
            <S.ScrollArrow>
              <S.Chevron />
              <S.Chevron />
              <S.Chevron />
            </S.ScrollArrow>
          </Fragment>} */}
          <S.Button isChecked={isChecked} onClick={isChecked ? openDrawer : this.expandCategory}>
            <span>{isChecked ? "Записаться" : "Подробнее"}</span>
            <ArrowIcon />
          </S.Button>
        </S.Container>
      </Fragment>
    );
  };
}


export default Block;
