import styled from 'styled-components';

export const Container = styled.div`
  width: 24%;
  background: rgba(0, 0, 0, 0.5);
  border: 0.5px solid #404040;
  display: flex;
  position: relative;
  flex-direction: column;
  align-items: center;
  color: white;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  text-align: ${({ isChecked }) => isChecked ? 'start' : 'center'};
  justify-content: space-between;
  padding: ${({ isChecked }) => isChecked ? '30px 14px' : '30px 0'};
  box-sizing: border-box;
  margin-top: 30px;
  /* svg {
    position: absolute;
    top: 20px;
  } */
  .xmark {
    position: absolute;
    top: 10px;
    right: 10px;
    width: 20px;
    height: 20px;
    fill: white;
    cursor: pointer;
  }
  @media (max-width: 960px) {
      min-height: fit-content;
      flex-direction: ${({ isChecked }) => isChecked ? 'column' : 'row'};
      width: 100%;
      margin-top: 15px;
      text-align: start;
      padding-left: 10px;
      padding: ${({ isChecked }) => isChecked ? '25px' : '0 10px'};
      svg {
        width: ${({ isChecked }) => isChecked ? '80px' : '40px'};
        height: ${({ isChecked }) => isChecked ? '80px' : '40px'};
        /* position: initial; */
      }
  }
`;

export const Button = styled.button`
  background: orange;
  border-radius: 20px;
  padding: 11px 28px;
  text-transform: uppercase;
  border: none;
  outline: none;
  /* position: absolute;
  bottom: 20px; */
  cursor: pointer;
    span {
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 19px;
      color: white;
    }
    svg {
      display: none;
    }
  @media (max-width: 960px) {
    background: ${({ isChecked }) => isChecked ? 'orange' : 'transparent'};;
    width: ${({ isChecked }) => isChecked ? 'min-content' : '10px'};
    border-radius: ${({ isChecked }) => isChecked ? '20px' : '0'};
    margin-top: ${({ isChecked }) => isChecked ? '60px' : '0'};
    position: initial;
    span {
      display: ${({ isChecked }) => isChecked ? 'block' : 'none'};;
    }
    svg {
      display: ${({ isChecked }) => isChecked ? 'none' : 'block'};
      fill: orange;
    }
  }
`;

export const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: ${({ isChecked }) => isChecked ? 'flex-start' : 'center'};
  max-height: 225px;
  overflow-y: scroll;
  padding: 0 10px;
  width: 100%;
  /* justify-content: space-evenly; */
  /* flex: 1; */
  ::-webkit-scrollbar {
        display: none;
      }
  @media (max-width: 960px) {
    max-height: fit-content;
    align-items: flex-start;
    padding: 15px 10px;
  }
`;

export const Title = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
  margin-bottom: 35px;
  @media (max-width: 960px) {
    margin-bottom: 7px;
  }
`;

export const ServiceTime = styled.div`
  margin-top: 5px;
  span {
    color: orange;
  }
`;

export const ScrollArrow = styled.div`
  justify-self: unset;
  position: relative;
  width: 24px;
  height: 50px;
`;

export const Chevron = styled.div`
  position: absolute;
  width: 15px;
  height: 3px;
  opacity: 0;
  transform: scale3d(0.5, 0.5, 0.5);
  animation: move 3s ease-out infinite;
  :first-child {
    animation: move 3s ease-out 1s infinite;
  }
  :nth-child(2) {
    animation: move 3s ease-out 2s infinite;
  }
  ::before, ::after {
    content: ' ';
    position: absolute;
    top: 0;
    height: 100%;
    width: 51%;
    background: #fff;
  }
  ::before {
    left: 0;
    transform: skew(0deg, 30deg);
  }
  ::after {
    right: 0;
    width: 50%;
    transform: skew(0deg, -30deg);
  }
  @keyframes move {
    25% {
    opacity: 1;
    }
    33% {
    opacity: 1;
    transform: translateY(30px);
    }
    67% {
    opacity: 1;
    transform: translateY(40px);
    }
    100% {
    opacity: 0;
    transform: translateY(55px) scale3d(0.5, 0.5, 0.5);
    }
  }
`;

export const ScrollText = styled.span`
  display: block;
  font-family: "Helvetica Neue", "Helvetica", Arial, sans-serif;
  font-size: 12px;
  color: #fff;
  text-transform: uppercase;
  white-space: nowrap;
  opacity: .25;
  animation: pulse 2s linear alternate infinite;
  @keyframes pulse {
  to {
  opacity: 1;
  }
  }
`;
