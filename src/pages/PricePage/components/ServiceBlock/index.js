import React, { Fragment } from 'react';

import * as S from './styled';

const ServiceBlock = ({ service, isLast }) => {
  const { serviceName, serviceDescription, price } = service;
  return (
    <Fragment>
      <S.Row>
        <S.Name>{serviceName}</S.Name>
        <S.Price>{price}</S.Price>
      </S.Row>
      <S.Description isLast={isLast}>{serviceDescription}</S.Description>
    </Fragment>
  );
};

export default ServiceBlock;
