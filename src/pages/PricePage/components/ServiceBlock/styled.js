import styled from 'styled-components';

export const Name = styled.span`
  font-weight: 600;
  font-size: 12px;
  line-height: 20px;
  color: white;
`;

export const Row = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
`;

export const Price = styled.span`
  color: orange;
  font-weight: 600;
  font-size: 12px;
  line-height: 20px;
`;

export const Description = styled.div`
  font-weight: normal;
  font-size: 11px;
  line-height: 20px;
  color: #C4C4C4;
  margin-bottom: ${({ isLast }) => isLast ? 0 : '20px'}
`;