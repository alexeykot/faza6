import React from 'react';
import BookingDrawer from 'components/BookingDrawer';
import { categories } from 'configs';

import Block from './components/Block';

import * as S from './styled';


class PricePage extends React.Component {

  state = {
    isDrawerOpen: false,
  }

  openDrawer = () => {
    if (window.innerWidth < 960) {
      window.location.href = "https://w236386.yclients.com/";
    } else {
      this.setState({ isDrawerOpen: true });
    }
  };

  closeDrawer = () => this.setState({ isDrawerOpen: false });

  render() {
    const { isDrawerOpen } = this.state;
    return (
      <S.Container>
        <BookingDrawer onClose={this.closeDrawer} isOpen={isDrawerOpen} />
        <S.BlockWrapper>
          {categories.map(category => (<Block openDrawer={this.openDrawer} category={category} />))}
        </S.BlockWrapper>
      </S.Container>
    );
  }
}

export default PricePage;
