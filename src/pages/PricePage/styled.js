import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-color: black;
  /* overflow-y: hidden; */
`;

export const BlockWrapper = styled.span`
  width: 90%;
  height: 65%;
  z-index: 2;
  display: flex;
  justify-content: space-between;
  @media (max-width: 960px) {
      flex-direction: column;
      height: 80%;
  }
`;