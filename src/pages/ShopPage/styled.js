import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;
`;

export const Title = styled.h1`
  color: white;
  z-index: 2;
  margin-bottom: 200px;
  font-style: normal;
  font-weight: 500;
  font-size: 40px;
  line-height: 47px;
`;