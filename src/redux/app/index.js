import { call, put, takeEvery } from 'redux-saga/effects';

// import http from 'services/http';

const LOAD_DISCOUNT_REQUEST = 'auth/LOAD_DISCOUNT_REQUEST';
const LOAD_DISCOUNT_SUCCESS = 'auth/LOAD_DISCOUNT_SUCCESS';
const LOAD_DISCOUNT_FAILURE = 'auth/LOAD_DISCOUNT_FAILURE';

const INITIAL_STATE = {
  loadedDiscount: false,
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case LOAD_DISCOUNT_REQUEST:
      return { ...INITIAL_STATE };
    case LOAD_DISCOUNT_SUCCESS:
      return { ...state, loadedDiscount: true };
    case LOAD_DISCOUNT_FAILURE:
      return { ...state, loadedDiscount: false };
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestLoadDiscount = () => ({ type: LOAD_DISCOUNT_REQUEST });

// <<<WORKERS>>>
function* loadDiscount() {
  try {
    yield put({ type: LOAD_DISCOUNT_SUCCESS });
  } catch (error) {
    console.log(error);
    yield put({ type: LOAD_DISCOUNT_FAILURE });
  }
}

// <<<WATCHERS>>>
export function* watchLoadDiscount() {
  yield takeEvery(LOAD_DISCOUNT_REQUEST, loadDiscount);
}
