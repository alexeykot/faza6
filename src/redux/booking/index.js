import { call, put, takeEvery } from 'redux-saga/effects';

// import http from 'services/http';

const TEST_REQUEST = 'auth/TEST_REQUEST';
const TEST_SUCCESS = 'auth/TEST_SUCCESS';
const TEST_FAILURE = 'auth/TEST_FAILURE';

const INITIAL_STATE = {
  test: false,
};

export default function reducer(state = INITIAL_STATE, action = {}) {
  switch (action.type) {
    case TEST_REQUEST:
      return { ...INITIAL_STATE, test: true };
    case TEST_SUCCESS:
      return { ...state, test: true };
    case TEST_FAILURE:
      return { ...state, test: false };
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestTest = () => ({ type: TEST_REQUEST });

// <<<WORKERS>>>
function* test() {
  try {
    console.log('test');
    yield put({ type: TEST_SUCCESS });
  } catch (error) {
    console.log(error);
    yield put({ type: TEST_FAILURE });
  }
}

// <<<WATCHERS>>>
export function* watchTest() {
  yield takeEvery(TEST_REQUEST, test);
}
