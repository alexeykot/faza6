import { combineReducers } from 'redux';

// import booking from './booking';
import app from './app';

const rootReducer = combineReducers({
 // booking,
  app,
});

export default rootReducer;
