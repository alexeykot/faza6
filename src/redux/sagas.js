import { all, fork } from 'redux-saga/effects';

// import * as testWatchers from './booking';
import * as appWatchers from './app';

export default function* root() {
  yield all([
    fork(appWatchers.watchLoadDiscount),
  ]);
}
