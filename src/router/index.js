import React from 'react';
import { Route, Switch, BrowserRouter as Router, Redirect } from 'react-router-dom';
import Menu from 'components/Menu';
import Footer from 'components/Footer'
import Background from 'components/Background';

import Main from './main';

const AppRouter = () => ([
  <Router key="Router">
    <Menu />
    <Background />
    <Switch>
      <Route exact path='/'  render={() => <Redirect to="/main" />} />
      <Route path='/' component={Main} />
    </Switch>
    <Footer />
  </Router>,
  // <Loader />
]);


export default AppRouter;
