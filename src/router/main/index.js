import React, { PureComponent } from 'react';
import { Route, Switch } from 'react-router-dom';

import PricePage from 'pages/PricePage';
import MainPage from 'pages/MainPage';
import PortfolioPage from 'pages/PortfolioPage';
import BarbersPage from 'pages/BarbersPage';
import ShopPage from 'pages/ShopPage';
import InteriorPage from 'pages/InteriorPage';
import NotFoundPage from 'pages/NotFoundPage';

class Main extends PureComponent {

  render() {
    return (
      <Switch>
        <Route exact path='/main' component={MainPage} />
        <Route exact path='/price' component={PricePage} />
        <Route exact path='/our-work' component={PortfolioPage} />
        <Route exact path='/barbers' component={BarbersPage} />
        <Route exact path='/shop' component={ShopPage} />
        <Route exact path='/interior' component={InteriorPage} />
        <Route component={NotFoundPage}/>
      </Switch>
    );
  }
}

export default Main;
