import axios from 'axios';

const baseURL = 'https://jogtracker.herokuapp.com/api/v1';

const http = axios.create({
  withCredentials: true,
  baseURL,
  headers: { 'Content-Type': 'application/json' },
});

http.interceptors.request.use(async (request) => {
  const token = '67b61593e7abbc63d37a3c7cf3a5a7eabd234e1801d54cdbeddf482c26ab27ea'; //MOCKED
  const newRequest = { ...request };
  if (token) {
    newRequest.headers.Authorization = `Bearer ${token}`;
  }
  return newRequest;
}, error => Promise.reject(error));

export default http;
